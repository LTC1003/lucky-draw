/**
 *  http://118.195.232.44:10020
*/
import axios from '@/server/axios'

// 奖项列表
export const queryAwards = (params) => {
  return axios({
    url: '/api/recruitment/luckywheel/queryAwards',
    method: 'get',
    params
  })
}

// 签到人员列表
export const queryPersonShow = (params) => {
  return axios({
    url: '/api/recruitment/luckywheel/queryPersonShow',
    method: 'get',
    params
  })
}

// 抽奖 {awardsId}
export const getLottery = (data) => {
  return axios({
    url: '/api/recruitment/luckywheel/getLottery',
    method: 'post',
    data,
  })
}

// 重置中奖名单 { awardsId, winningPersonId }
export const resetPrizeResult = (params) => {
  return axios({
    url: '/api/recruitment/luckywheel/resetPrizeResult',
    method: 'get',
    params,
  })
}

// 选择奖项获取中奖人数据 { awardsId } 查询
export const countNameShow = (params) => {
  return axios({
    url: '/api/recruitment/luckywheel/countNameShow',
    method: 'get',
    params,
  })
}

// 刷新奖项 { awardsId }
export const resetAwardsAll = (data) => {
  return axios({
    url: '/api/recruitment/luckywheel/resetAwardsAll',
    method: 'post',
    data,
  })
}
// 刷新全部奖项
export const resetAll = (data) => {
  return axios({
    url: '/api/recruitment/luckywheel/resetAll',
    method: 'post',
    data,
  })
}

// 选择抽奖人数
// export const setExtractAmount = (data) => {
//   return axios({
//     url: '/api/recruitment/luckywheel/setExtractAmount',
//     method: 'post',
//     data,
//   })
// }