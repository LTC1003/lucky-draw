const path = require('path')

module.exports = {
  // publicPath: {Type: string, Default: '/'};
  // publicPath: process.env.NODE_ENV === 'production' ? '/production-sub-path/' : '/',
  publicPath: process.env.NODE_ENV === 'production' ? './' : '/',
  // outputDir: {Type: string, Default: 'dist'}(当运行 vue-cli-service build 时生成的生产环境构建文件的目录。);
  outputDir: 'dist',
  // assetsDir: {Type: string, Default: 'assets'}(放置生成的静态资源 (js、css、img、fonts) 的 (相对于 outputDir 的) 目录);
  // assetsDir: '',
  // indexPath: {Type: string, Default: 'index.html'};
  indexPath: 'index.html',
  // eslint-loader 是否在保存的时候检查 安装@vue/cli-plugin-eslint有效, lintOnSave: true; lintOnSave 是一个 truthy 的值时,开发和生产构都会被启用.在生产构建禁用
  // lintOnSave: process.env.NODE_ENV !== 'production',
  lintOnSave: false,
  // pages: {Type: object, Default: undefined};
  pages: {
    index: {
      // page 的入口
      entry: 'src/main.js',
      // 模板来源
      template: 'public/index.html',
      // 在 dist/index.html 的输出
      filename: 'index.html',
      // 当使用 title 选项时，
      // template 中的 title 标签需要是 <title><%= htmlWebpackPlugin.options.title %></title>
      title: ' 中企动力集团年会',
      // 在这个页面中包含的块，默认情况下会包含
      // 提取出来的通用 chunk 和 vendor chunk。
      // chunks: ['chunk-vendors', 'chunk-common', 'index']
    },
  },
  //可以在正式环境下关闭错误报告 console.log...
  configureWebpack: config => {  
    if (process.env.NODE_ENV === 'production') {
      // 为生产环境修改配置... production
      config.mode = "production";
    } else {
      // 为开发环境修改配置... development
      config.mode = "development";
    }
  },
  // webpack-dev-server 相关配置
  devServer: { // 设置代理
    // host: '127.0.0.1',
    // port: 8080,
    // https: true,
    open: true, // 默认打开浏览器: false  
    hot: true, //热加载
    proxy: { // 线上测试接口
      '/api': {                                    
        target: 'http://118.195.232.44:10020',
        // ws: true,
        changeOrigin: true,
        pathRewrite: {
          '^/api': ''
        }
      },
    },
  },
}
