// const baseUrl = 'http://118.195.232.44:10020'

module.exports = {
  // baseUrl: baseUrl,
  method: 'get',
  // 基础url前缀
  // 请求头信息Content-Type 默认设置json
  headers: {
    'Content-Type': 'application/json'
  },
  // 参数
  data: {},
  // 在超时前, 所有请求都会等待2500ms
  timeout: 2500,
  // 携带凭证
  withCredentials: false,
  // 返回数据类型
  responseType: 'json'
}
