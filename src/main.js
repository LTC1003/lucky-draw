


import { createApp } from "vue"
import Antd from "ant-design-vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import "ant-design-vue/dist/antd.css";

import api from "@/server/api"; //接口api
// import "./routerPermission.js"; //路由守卫

const app = createApp(App);
app.config.globalProperties.$API = api;
app.use(store).use(router).use(Antd).mount("#app");

// def int create
// createApp(App).use(store).use(router).use().mount('#app')
