import { createRouter, createWebHistory, createWebHashHistory } from 'vue-router'
import Home from '../views/Home.vue'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/screen',
    name: 'Screen',
    // route level code-splitting
    // this generates a separate chunk (Screen.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "Screen" */ '../views/Screen.vue'),
  },
  {
    path: '/result',
    name: 'Result',
    component: () => import('@/views/Result.vue'),
  }
]

const router = createRouter({
  // history: createWebHistory(process.env.BASE_URL),
  history: createWebHashHistory(),
  routes
})

export default router
